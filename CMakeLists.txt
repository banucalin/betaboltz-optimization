cmake_minimum_required(VERSION 3.0)

set(This betaboltz-benchmark)

project(${This} VERSION 0.1.0 LANGUAGES CXX)

set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${CMAKE_SOURCE_DIR}/cmake/Modules/")

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall -Wextra -pedantic -Wno-unused-parameter" )

include_directories(${PROJECT_SOURCE_DIR}/include)
include_directories(${PROJECT_SOURCE_DIR}/external/cli11/include)

add_executable(${This} src/main.cpp)

# set(CMAKE_CXX_FLAGS  "${CMAKE_CXX_FLAGS} -O0 -gdwarf-3")

set(CMAKE_INSTALL_PREFIX "${CMAKE_SOURCE_DIR}/zcross")
set(CMAKE_CUDA_COMPILER /usr/local/cuda/bin/nvcc)

add_subdirectory(zcross)
add_subdirectory(betaboltz)
add_dependencies(betaboltz zcross)

get_target_property(BETABOLTZ_BINARY_DIR betaboltz BINARY_DIR)

include_directories(${BETABOLTZ_BINARY_DIR})
target_link_libraries(${This} DFPE::betaboltz)
