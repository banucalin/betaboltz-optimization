# Introduction

This page contains a set of cross section data from different sources (as now, only [LXCat](www.lxcat.net)) in a custom machine readable XML format.

This format was developed to improve the reading of cross section data from physics softare (i.e. [ZCross](https://gitlab.com/micrenda/zcross) and [Betaboltz](http://gitlab.com/micrenda/betaboltz)) but you can use it to integrate in your software too.

The benefit of this format are:
1. Well defined schema (see [zcross.xsd](zcross.xsd) )
2. Charge and mass conservation checked
3. Complete bigliography for each table (including [doi](https://www.doi.org/))

All the file existing in this catalog are **property of their respective authors** and are subject to **speficic license**.

For [lxcat](lxcat/) data please refer to [how to reference](https://fr.lxcat.net/instructions/how_reference.php) page and the [policy](https://fr.lxcat.net/instructions/redistribution.php) about the redistribution of data by third parties.