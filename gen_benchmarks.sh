#!/bin/bash

if [[ $EUID -ne 0 ]]; then
   echo "This script must be run as root" 
   exit 1
fi

INTEL_PATH=/opt/intel
VTUNE_PATH=$INTEL_PATH/vtune_profiler/bin64/vtune
CMAKE=cmake
BUILD_TYPE=Release
BUILD_TOOL=Ninja
BASE_DIR=.
LOG_DIR=$BASE_DIR/log

source $INTEL_PATH/vtune_profiler/vtune-vars.sh >> /dev/null

#if [ "$1" == "clang" ]; then
#    echo "Using clang compiler"
#    C_COMPILER=clang
#    CXX_COMPILER=clang++
#    SUB_DIR=clang
#elif [ "$1" == "icc" ]; then
#    echo "Using intel compiler"
#    C_COMPILER=icc
#    CXX_COMPILER=icpc
#    SUB_DIR=intel
#elif [ "$1" == "gcc" ]; then
#    echo "Using gnu compiler"
#    C_COMPILER=gcc
#    CXX_COMPILER=g++
#    SUB_DIR=gnu
#else
#    echo "Compiler not specified!"
#    exit -1
#fi

#$C_COMPILER --version
#$CXX_COMPILER --version

#BUILD_DIR=$BASE_DIR/build/$SUB_DIR
RESULT_DIR=$BASE_DIR/benchmark_results/$SUB_DIR

#$CMAKE -H. -B$BUILD_DIR -DCMAKE_CXX_COMPILER=$CXX_COMPILER -DCMAKE_C_COMPILER=$C_COMPILER -G $BUILD_TOOL -DENABLE_CUDA=ON
#ninja -C $BUILD_DIR clean
#ninja -C $BUILD_DIR
#rm -rf $RESULT_DIR

export ZCROSS_DATA=/home/banucalin/Projects/betaboltz-optimization/zcross_data

BUILD_DIR=$BASE_DIR/build/reldeb
APP=betaboltz-benchmark

$VTUNE_PATH -collect hotspots           -finalization-mode=full                                                                 -r $RESULT_DIR/vtune_hs_$SUB_DIR  -- $BUILD_DIR/$APP
# $VTUNE_PATH -collect memory-consumption -finalization-mode=full                                                                 -r $RESULT_DIR/vtune_mc_$SUB_DIR  -- $BUILD_DIR/$APP
# $VTUNE_PATH -collect memory-access      -finalization-mode=full                                                                 -r $RESULT_DIR/vtune_ma_$SUB_DIR  -- $BUILD_DIR/$APP
# $VTUNE_PATH -collect threading          -finalization-mode=full                                                                 -r $RESULT_DIR/vtune_th_$SUB_DIR  -- $BUILD_DIR/$APP
# $VTUNE_PATH -collect hpc-performance    -finalization-mode=full -knob enable-stack-collection=true -knob collect-affinity=true  -r $RESULT_DIR/vtune_hpc_$SUB_DIR -- $BUILD_DIR/$APP
# $VTUNE_PATH -collect uarch-exploration  -finalization-mode=full -knob collect-memory-bandwidth=true                             -r $RESULT_DIR/vtune_uae_$SUB_DIR -- $BUILD_DIR/$APP

chmod -R 777 $RESULT_DIR
#chmod -R 777 $BUILD_DIR

#$BUILD_DIR/cpu-benchmark