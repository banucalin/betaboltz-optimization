#include <Betaboltz.hpp>

using namespace dfpe;
using namespace boost::units;
using namespace std;

int main()
{
    BetaboltzSimple beta;

	 InteractionBulletLimiter limiter(10000);
     beta.addLimiter(limiter);
	
	 beta.setSeed(0);

    beta.setDisableCheckMonoatomic(true);

    // Set the total density of the gas mixture
    QtySiNumberDensity      density = 2.6867811e25 / (si::meter * si::meter * si::meter); //  * pressure / QtySiPressure(101.325 * si::kilo * si::pascal);

    // Here we describe the gas mixture
    GasMixture gas;

    // Nitrogen,       N2,                 78.084
    // Oxygen,         O2,                 20.947
    // Argon,          Ar,                  0.934
    // Carbon dioxide, CO2,                 0.0350
    // Neon,           Ne,                  0.001818
    // Helium,         He,                  0.000524
    // Methane,        CH4,                 0.00017
    // Wather,         H2O                  variable according air umidity. Other gas must be reduced corrispetively.

    QtySiNumberDensity densityN2  (78.084    * density);
    QtySiNumberDensity densityO2  (20.947    * density);
    QtySiNumberDensity densityAr  ( 0.934    * density);
    QtySiNumberDensity densityCO2 ( 0.0350   * density);
    QtySiNumberDensity densityNe  ( 0.001818 * density);
    QtySiNumberDensity densityHe  ( 0.000524 * density);
    QtySiNumberDensity densityCH4 ( 0.00017  * density);


    gas.addComponent("N2",   densityN2);
    gas.addComponent("O2",   densityO2);
    gas.addComponent("Ar",   densityAr);
    gas.addComponent("CO2",  densityCO2);
    gas.addComponent("Ne",   densityNe);
    gas.addComponent("He",   densityHe);
    gas.addComponent("CH4",  densityCH4);

    beta.enableProcess("e", "N2",  "Biagi");
    beta.enableProcess("e", "O2",  "Biagi");
    beta.enableProcess("e", "Ar",  "Biagi");
    beta.enableProcess("e", "CO2", "Biagi");
    beta.enableProcess("e", "Ne",  "Biagi");
    beta.enableProcess("e", "He",  "Biagi");
    beta.enableProcess("e", "CH4", "Morgan");


    // Setting a 40 kV/cm electric field aligned to z axis
    UniformFieldRelativisticChin field(
        VectorC3D<QtySiElectricField>(
            QtySiElectricField( 0. * si::kilo * si::volts / cgs::centimeter),
            QtySiElectricField( 0. * si::kilo * si::volts / cgs::centimeter),
            QtySiElectricField(40. * si::kilo * si::volts / cgs::centimeter)));

    // Create a detector with infinite volume
    VectorC3D<QtySiLength> size(
        0.01 * si::meter,
        0.01 * si::meter,
        0.000128 * si::meter);

    BoxDetector detector(gas, field, size);
    beta.setDetector(detector);

    // This is the seed electron
    ElectronSpecie electronSpecie;

    // This starts the simulation, with an electron seed at position (0,0,0)
    beta.execute(electronSpecie);

    // Printing our efficency report
    cout << beta.getStatsEfficency() << endl;
}
